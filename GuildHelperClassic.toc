## Interface: 11302
## Title: |cff55ae00Guild Helper Classic|r
## Notes: Guild helper addon allowing guild members to share character information. Displays guild class summary as well!
## Version: @project-version@
## Author: Copperbolts, Forestsage, Stephen
## SavedVariables: GuildHelperClassicDB, GhcDb
## OptionalDeps: Ace3
# X-Embeds: Ace

## X-Curse-Project-ID: 340063

# Libraries
embeds.xml

# Localization
# localization.xml

GHC_Db.lua

# Modules
Modules/Bank.lua
Modules/Roster.lua
Modules/Player.lua
Modules/EventHandler.lua
Config.lua

# Core
Core.lua

# UI
GHC_NewUI.xml
GHC_UI.lua
