--[==[

Copyright ©2019 Samuel Thomas Pain

The contents of this addon, excluding third-party resources, are
copyrighted to their authors with all rights reserved.

This addon is free to use and the authors hereby grants you the following rights:

1.     You may make modifications to this addon for private use only, you
    may not publicize any portion of this addon.

2.     Do not modify the name of this addon, including the addon folders.

3.     This copyright notice shall be included in all copies or substantial
    portions of the Software.

All rights not explicitly addressed in this license are reserved by
the copyright holders.

]==]--


local ADDON_NAME, GHC = ...

local DEBUG = false
--@alpha@
DEBUG = true
--@end-alpha@

GHC_Core = LibStub("AceAddon-3.0"):NewAddon(GHC.addon_name, "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0", "AceSerializer-3.0", "AceBucket-3.0", "AceTimer-3.0")
GHC_Core.Name = ADDON_NAME
GHC_Core.LocName = select(2, GetAddOnInfo(GHC_Core.Name))
GHC_Core.Notes = select(3, GetAddOnInfo(GHC_Core.Name))

local tinsert, tconcat, tremove, tsort = table.insert, table.concat, table.remove, table.sort

GHC.Functions = {}

GHC_Core.Guildspace = nil


-------------------------------------------------------------------------------
-- HELPER FUNCTIONS
-------------------------------------------------------------------------------

--- Deep copy of all entries in `...` sources to `dest`
-- Entries in `dest` will be overwritten by entries in the `...` sources if they have the same key.
-- Later sources' entries will overwrite earlier ones.
--
-- WARNING: assign({a = 1}, "erase") == "erase"
function GHC_Core:assign(dest, ...)
    for i = 1, select("#", ...) do
        local varg = select(i, ...)
        if type(varg) == "table" then
            for k, v in pairs(varg) do
                dest[k] = self:assign(dest[k] or {}, v)
            end
        else  -- number, string, boolean, etc.
            dest = varg
        end
    end
    return dest
end


--- Return a table of keys sorted by `sortFunc` that can be used to index into the passed table
function GHC_Core:getSortedKeysByValue(tbl, sortFunc)
    local keys = {}
    for k in pairs(tbl) do
        tinsert(keys, k)
    end
    tsort(keys, function(a, b) return sortFunc(tbl[a], tbl[b]) end)
    return keys
end


function GHC_Core:CreateMemberTable(...)
    local member = {
        Name = nil,
        Level = nil,
        Class = nil,
        Gender = nil,
        Race = nil,
        -- TODO Primary/secondary can be inferred from skill name directly
        Professions = {
            Primary = {
                -- { Name = nil, Level = nil, },
                -- { Name = nil, Level = nil, },
            },
        },
        Role = nil,
        PublicNote = nil,
        MainsName = nil,
    }
    return self:assign(member, ...)
end


function GHC_Core:tprint(tbl)
    if not tbl then return end
    local ret = {}
    for k, v in pairs(tbl) do
        if type(v) == "table" then
            tinsert(ret, ("%q = { %s }"):format(k, self:tprint(v)))
        else
            tinsert(ret, ("%q=%q"):format(k, v))
        end
    end
    return tconcat(ret, ", ")
end


function GHC_Core:keyCount(tbl)
    local i = 0
    for _ in pairs(tbl) do i = i + 1 end
    return i
end


--- Debug function to find event args
-- function GHC_Core:GetArgs(...)
--     for i = 1, select("#", ...) do
--         arg = select(i, ...)
--         print(i.." "..tostring(arg))
--     end
-- end


function GHC_Core:migrateOldGuildspace(guildName)
    local newGuildspace = GHC_Core.db.factionrealm[guildName]

    -- Only allow migration once
    if not newGuildspace then return end

    -- Attempt to migrate old data
    if GhcDb then
        local oldGuildspace = GhcDb[guildName]
        if oldGuildspace then
            GHC_Core:Print("Upgrading data for", guildName)
            if oldGuildspace.GuildDb then
                for _, member in ipairs(oldGuildspace.GuildDb) do
                    -- name, level class, public note can be retrieved during ScanGuildRoster
                    newGuildspace.Roster[member.GUID] = self:CreateMemberTable({
                        Race = member.Race,
                        Gender = member.Gender,
                        Role = tonumber(member.Role),
                        MainsName = member.MainsName,
                    })
                    local p = 1
                    if member.Profession1Name and member.Profession1Level then
                        newGuildspace.Roster[member.GUID].Professions.Primary[p] = {
                            Name = member.Profession1Name,
                            Level = member.Profession1Level,
                        }
                        p = p + 1
                    end
                    if member.Profession2Name and member.Profession2Level then
                        newGuildspace.Roster[member.GUID].Professions.Primary[p] = {
                            Name = member.Profession2Name,
                            Level = member.Profession2Level,
                        }
                        p = p + 1
                    end
                end
            end
            if oldGuildspace.GuildBank then
                newGuildspace.Bank = self:assign({}, oldGuildspace.GuildBank)
            end
            if oldGuildspace.GuildBankMoney then
                newGuildspace.Money = oldGuildspace.GuildBankMoney
            end

            -- TODO Finally, wipe out the old data
            -- GhcDb[guildName] = nil
        end
    end
end


-------------------------------------------------------------------------------
-- GUI MANAGEMENT FUNCTIONS
-------------------------------------------------------------------------------

function GHC_OnLoad(frame)
    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end


--- Set a frame to be moveable
function GHC.Functions.MakeFrameMove(frame)
    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end


--- Used for roster professions icons
function GHC.Functions.ShowTooltip(frame, text)
    GHC.UI.Tooltip:SetParent(frame)
    GHC.UI.Tooltip.Text:SetText(text)
    local w = GHC.UI.Tooltip.Text:GetStringWidth()
    GHC.UI.Tooltip:SetSize(w + 24, 30)
    GHC.UI.Tooltip:SetPoint("TOPLEFT", - w * 1.25, 12)
    GHC.UI.Tooltip:Show()
end


function GHC.Functions.ScrollByAmount(scrollBar, delta)
    local value = scrollBar:GetValue() - delta
    -- GHC_Core:Debug(scrollBar:GetValue(), delta, value)
    scrollBar:SetValue(value)
end


-------------------------------------------------------------------------------
-- DATA MANAGEMENT FUNCTIONS
-------------------------------------------------------------------------------

--- Used as a nuclear option to wipe and restore default db
function GHC.Functions.Reset(guildName)
    if not GHC_Core.Guildspace then
        GHC_Core:Print("Either you are not in a guild or the guild information is not available; please try opening the guild window and run reset again.")
        return
    end

    local gName = GHC_Core:GetPlayerGuildInfo()
    if guildName == gName then
        GHC_Core:Print("Found matching guild name, creating new settings")
        GHC_Core:ClearGuildspace(guildName)
        GHC_Core.Guildspace = GHC_Core:GetGuildspace()
    else
        GHC_Core:Print("Unable to find matching guild, please make sure guild name entered corectly")
    end
end


function GHC_Core:UpdateClassSummary()
    if not GHC_Core.Guildspace then return end

    -- Update the class summary, reset counts first then loop db and make new count > sort > update UI
    GHC_Core.Guildspace.ClassCount = {}
    for _, member in pairs(GHC_Core.Guildspace.Roster) do
        GHC_Core.Guildspace.ClassCount[member.Class] = (GHC_Core.Guildspace.ClassCount[member.Class] or 0) + 1
    end

    GHC_Core.Guildspace.ClassCountKeysSorted = GHC_Core:getSortedKeysByValue(
        GHC_Core.Guildspace.ClassCount, function (a, b) return a > b end
    )

    local totalMembers = GetNumGuildMembers()
    for i, class in ipairs(GHC_Core.Guildspace.ClassCountKeysSorted) do
        local count = GHC_Core.Guildspace.ClassCount[class]
        local bar = GHC.UI.ClassSummaryBars[i]
        bar:Show()
        bar.StatusBar:SetValue((count / totalMembers) * 100.0)
        bar.StatusBar:SetStatusBarColor(GHC.Db.ClassColours[class].r, GHC.Db.ClassColours[class].g, GHC.Db.ClassColours[class].b)
        bar.Icon.Texture:SetTexture(("Interface/Addons/GuildHelperClassic/ClassIcons/%s"):format(class))
        bar.Text:SetText(("%d [%.1f%%]"):format(count, (count / totalMembers) * 100.0))
    end
    for i = #GHC_Core.Guildspace.ClassCountKeysSorted + 1, #GHC.Db.ClassIDs do
        GHC.UI.ClassSummaryBars[i]:Hide()
    end
end


function GHC_SummaryFrameOnShow()
    GHC_Core:UpdateClassSummary()
end


function GHC_BankFrameOnShow(frame)
    GHC_Bank:UpdateGuildBankGridView(1)
end


function GHC_RosterFrameOnShow()
    GHC_Roster.isRosterFrameDisplayDirty = true
    GHC_Roster:ScanGuildRoster()
end
function GHC_RosterFrameOnHide()
end
-- GHC_Core:Print('loaded scripts')


-------------------------------------------------------------------------------
-- CORE
---------------------------------------------------

--- RegisterGuildspace only returns true the first time it successfully registers the Guildspace
function GHC_Core:RegisterGuildspace(guildName)
    -- self:Debug("RegisterGuildspace", UnitGUID("player"), guildName, self.Guildspace ~= nil)
    if not guildName then return end

    if self.Guildspace then return false end

    -- If the Guildspace db exists, then success, otherwise need to create new default table
    self.Guildspace = self:GetGuildspace()
    if self.Guildspace then return true end

    local success = self:ClearGuildspace(guildName)
    if not success then return false end

    local newGuildspace = self:GetGuildspace()
    if not newGuildspace then return false end

    self:migrateOldGuildspace(self:GetPlayerGuildInfo())

    self.Guildspace = self:GetGuildspace()
    return self.GetGuildspace ~= nil
end


function GHC_Core:ClearGuildspace(guildName)
    -- self:Debug("ClearGuildspace", UnitGUID("player"), guildName)
    if not guildName then return false end

    self.Guildspace = nil
    self.db.factionrealm[guildName] = {
        Roster = {},
        RosterKeysSorted = {},
        RosterBackup = {},
        BankTimestamp = nil,
        Bank = {},
        Money = 0,
        ClassCount = {},
        ClassCountKeysSorted = {},
    }
    return true
end


function GHC_Core:GetGuildspace(guildName)
    if self.Guildspace then return self.Guildspace end

    local numGuildMembers = GetNumGuildMembers()
    if not numGuildMembers or numGuildMembers <= 0 then return nil end

    guildName = guildName or self:GetPlayerGuildInfo()
    -- self:Debug("GetGuildspace", UnitGUID("player"), guildName, self.isGuildspaceRegistered)
    if not guildName then return nil end

    return self.db.factionrealm[guildName]
end
----------------------------

--- TODO Consider caching the value and only change if the player leaves or joins a guild (register new event)
function GHC_Core:GetPlayerGuildInfo()
    -- self:Debug("GetPlayerGuildInfo", IsInGuild("player"), GetGuildInfo("player"))
    return IsInGuild("player") and GetGuildInfo("player") or nil
end


function GHC_Core:ChatCommand(msg)
    if msg == nil or msg == "" then
        msg = "log"
    end
    local commands = {
        -- ["help"] = function() self:Print("Available commands are: open, scangb, fix, help") end,
        ["open"] = function()
            GHC.UI.Toggle()
        end,
        ["fix"] = function()
            StaticPopup_Show("GHC_FixMe")
        end,
        ["scangb"] = function()
            if GHC_Bank:ScanGuildBank() ~= nil then
                GHC_Bank:SendGuildBankData()
            end
        end,
        --@debug@
        ["debug"] = function()
            DEBUG = not DEBUG
            self:Print("DEBUG =", DEBUG)
        end,
        --@end-debug@
    }
    local prefix, nextposition = self:GetArgs(msg, 1)
    local cmd = commands[prefix]
    if cmd ~= nil then
        cmd()
    else
        local message = self:GetArgs(msg, 1, nextposition)
        if DEBUG and prefix ~= nil and message ~= nil then
            self:Print("GHC_Slash: cmd:", prefix, message)
            self:SendCommMessage(prefix, message, "GUILD")
        else
            self:Print("Available commands are: open, scangb, fix, help")
        end
    end
end


function GHC_Core:Debug(...)
    --@debug@
    if DEBUG then
        self:Print(...)
    end
    --@end-debug@
end


function GHC_Core:Debugf(formatString, ...)
    --@debug@
    if DEBUG then
        self:Printf(formatString, ...)
    end
    --@end-debug@
end


--- Called after Guildspace has been registered
function GHC_Core:Load()
    -- Create player settings db
    -- if not self.db.char then
    --     assign(
    --         self.db.char,
    --         CreateMemberTable(),
    --         {
    --             Name = UnitName("player"),
    --             Role = 0,
    --         }
    --     )
    -- end
    local global = self.db.global
    global.AutoSendCharacterData = self.db.global.AutoSendCharacterData or true
    global.CurrentVersion = GetAddOnMetadata("GuildHelperClassic", "Version")
    global.NewVersion = self.db.global.NewVersion or self.db.global.CurrentVersion

    self:AddonUpdater()
end


-- TODO CurrentVersion is never updated
function GHC_Core:AddonUpdater()
    if self.db.global.NewVersion ~= self.db.global.CurrentVersion then
        -- StaticPopup_Show("GHC_UpdatePopup")
    end
end



--- This function is only called during the ADDON_LOADED event.
function GHC_Core:OnInitialize()
    self:RegisterChatCommand("ghc", "ChatCommand")

    self.db = LibStub("AceDB-3.0"):New("GuildHelperClassicDB", {
        global = {
            minimap = {
                hide = false,
            },
            AutoSendCharacterData = true,
        },
    })

    GHC.UI.MinimapIcon:Register(GHC.addon_name, GHC.UI.MinimapButton, self.db.global.minimap)
end


-- The OnEnable() and OnDisable() methods of your addon object are called by AceAddon when
-- your addon is enabled/disabled by the user.
-- Unlike OnInitialize(), this may occur multiple times without the entire UI being reloaded.

-- This function is only called during the PLAYER_LOGIN event, or during ADDON_LOADED,
-- if IsLoggedIn() already returns true at that point, e.g. for Load-on-Demand Addons.
--      IsLoggedIn() returns nil before the PLAYER_LOGIN event has fired, 1 afterwards.
function GHC_Core:OnEnable()
    -- restore saved settings
    -- register events
    -- register library callbacks
    -- check Saved Variables for frames' OnShow
    -- or other "on" setup
    GHC.UI.DrawGuildRosterListview()
    GHC.UI.DrawGuildClassSummaryBars()
    GHC.UI.DrawGuildBankGridView()
    -- GHC.UI.PlayerOptionsFrame.PlayerRoleDropdown_Init()
    GHC.UI.RosterScrollFrame.FilterProfDropDown_Init()
    GHC.UI.RosterScrollFrame.FilterRoleDropDown_Init()

    GHC.Functions.MakeFrameMove(GHC.UI.ParentFrame)

    GHC_EventHandler:RegisterComm("ghc-update-me",  "UpdateMeCommHandler")
    GHC_EventHandler:RegisterComm("ghc-playerdata", "PlayerDataCommHandler")
    GHC_EventHandler:RegisterComm("ghc-gbdata-s",   "GuildBankMoneyCommHandler")
    GHC_EventHandler:RegisterComm("ghc-gbdata",     "GuildBankDataCommHandler")
    GHC_EventHandler:RegisterComm("ghc-gbdata-f",   "GuildBankFinishCommHandler")

    GHC_EventHandler:RegisterEvent("GUILD_ROSTER_UPDATE")  -- GetGuildInfo("player") available
    GHC_EventHandler:RegisterEvent("GUILD_RANKS_UPDATE")  -- GetGuildInfo("player") definitely available
    -- GHC_EventHandler:RegisterEvent("PLAYER_GUILD_UPDATE")  -- Reset Guildspace

    -- Combat detection
    -- GHC_EventHandler:RegisterEvent("PLAYER_REGEN_DISABLED")
    GHC_EventHandler:RegisterEvent("PLAYER_REGEN_ENABLED")

    -- GHC_EventHandler:RegisterEvent("SKILL_LINES_CHANGED")
    -- GHC_EventHandler:RegisterBucketEvent({"CHAT_MSG_SKILL", "PLAYER_LEVEL_UP"}, 6, "PlayerDataChangedHandler")
    -- TODO Don't both with PLAYER_LEVEL_UP since Player Level can be acquired by all guild members anyway?
    GHC_EventHandler:RegisterBucketEvent("CHAT_MSG_SKILL", 60, "PlayerDataChangedHandler")
    -- GHC_Core:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED", function(...) print(...) end)
end


function GHC_Core:OnDisable()
    -- AceEvent-3.0 automatically handles unregistering events
end
