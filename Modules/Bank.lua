local ADDON_NAME, GHC = ...
GHC_Bank = {...}  -- Public interface
-- local Bank = {...}  -- Private interface

local tinsert = table.insert


-- TODO Someone reported the event based isBankOpen detection does not always work...
-- TODO Might have to do with Auctioneer?  Didn't work after Auctioneer scan...
-- HACK GetContainerNumSlots appears to return just 0 when the BankFrame is closed and (free, 0) when it's open
local function IsBankOpen()
    local _, bagType = GetContainerNumFreeSlots(BANK_CONTAINER)
    return GHC_Bank.isBankOpen or bagType ~= nil
end


function GHC_Bank:ScanGuildBank()
    -- GHC_Core:Print("ScanGuildBank", IsBankOpen())

    if self.areGuildBankEventsRegistered then
        if not IsBankOpen() then
            GHC_Core:Print("NOTE: You must have the Bank open in order to scan Guild Bank data")
            return
        end

        local guildspace = GHC_Core.Guildspace
        if not guildspace then return end

        GHC_Core:Print("Scanning Guild Bank...")

        guildspace.Bank = {}
        guildspace.Money = GetMoney()
        guildspace.BankTimestamp = GetServerTime()

        local c = 0
        -- Scan basic bank slots
        for i = 1, NUM_BANKGENERIC_SLOTS do
            -- icon, itemCount, locked, quality, readable, lootable, itemLink, isFiltered, noValue, itemID
            local _, itemCount, _, _, _, _, itemLink, _, _, itemID = GetContainerItemInfo(BANK_CONTAINER, i)
            if itemLink then
                -- TODO Enchant data is embedded in the item link
                -- print(string.gsub(itemLink, "\124", "\124\124"))
                tinsert(guildspace.Bank, {ID = itemID, Count = itemCount})
                c = c + 1
            end
        end

        -- Scan additional bags in bank slots
        for bag = 5, 11 do
            for slot = 1, GetContainerNumSlots(bag) do
                local _, itemCount, _, _, _, _, itemLink, _, _, itemID = GetContainerItemInfo(bag, slot)
                if itemLink then
                    tinsert(guildspace.Bank, {ID = itemID, Count = itemCount})
                    c = c + 1
                end
            end
        end

        -- Scan character bags
        for bag = 0, 4 do
            for slot = 1, GetContainerNumSlots(bag) do
                local _, itemCount, _, _, _, _, itemLink, _, _, itemID = GetContainerItemInfo(bag, slot)
                if itemLink then
                    tinsert(guildspace.Bank, {ID = itemID, Count = itemCount})
                    c = c + 1
                end
            end
        end
        GHC_Core:Printf("Finished Scanning Guild Bank (%d items)", c)

        return c
    else
        GHC_Core:Printf(
            "WARNING: The Guild Bank character must be ranked %q, %q, or %q, or be listed in the Guild Information window, and must have its Character Role set to %q",
            GuildControlGetRankName(2), GuildControlGetRankName(1),  -- These are 1-indexed
            "Guild Bank", "Guild Bank"
        )
        GHC_Core:Print("If you were recently promoted or listed in the Guild Information window as a Guild Bank alt then you must log out and back in")
    end
end


function GHC_Bank:SendGuildBankData()
    -- GHC_Core:Debugf(
    --     "SendGuildBankData: pendingTransaction = %s, ResetTimer = %s",
    --     tostring(self.pendingTransaction ~= nil),
    --     tostring(self.sendGuildBankDataResetTimer ~= nil)
    -- )

    -- Don't send data if in the middle of receiving new data
    if self.pendingTransaction then
        GHC_Core:Print("New data is being received - please try again in a few seconds")
        return
    end

    if self.sendGuildBankDataResetTimer then
        GHC_Core:Print("Please wait a few seconds before sending again to avoid overloading the server")
        return
    end

    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    -- Don't send data if we don't have a valid timestamp
    if not guildspace.BankTimestamp then
        GHC_Core:Print("Will not send data without timestamp")
        return
    end

    GHC.UI.GuildBankParentFrame.SendBankDataButton:Disable()

    GHC_Core:Print("Sending Guild Bank Data...")

    -- GHC_Core:Debug("SEND ghc-gbdata-s", tostring(guildspace.Money), "GUILD")
    GHC_Core:SendCommMessage("ghc-gbdata-s", tostring(guildspace.Money), "GUILD")

    local tbl = {}
    for _, item in ipairs(guildspace.Bank) do
        tinsert(tbl, {ID = item.ID, Count = item.Count})
    end

    local ds = GHC_Core:Serialize(tbl, guildspace.BankTimestamp)

    -- GHC_Core:Debug("SEND ghc-gbdata", ds, "GUILD")
    GHC_Core:SendCommMessage("ghc-gbdata", ds, "GUILD", nil, nil,
        function(arg1, numBytesSent, numBytesTotal)
            GHC_Core:Debug("Sent", numBytesSent, "/", numBytesTotal)
            if numBytesSent == numBytesTotal then
                GHC_Core:Printf("Finished Sending Guild Bank Data (%d items)", #tbl)

                -- GHC_Core:Debug("SEND ghc-gbdata-f", "blank", "GUILD")
                GHC_Core:SendCommMessage("ghc-gbdata-f", "blank", "GUILD")
            end
        end
    )

    self.sendGuildBankDataResetTimer = GHC_Core:ScheduleTimer(
        function(self, ...)
            -- GHC_Core:Debug("ScanGuildBankResetTimer", ...);
            GHC_Core:Print("Sending Guild Bank Data timed out (60s), please try again")
            self.sendGuildBankDataResetTimer = nil;
            self.pendingTransaction = nil;
            GHC.UI.GuildBankParentFrame.SendBankDataButton:Enable();
        end, 60, self
    )
end


function GHC_Bank:GuildBankStart(money, sender)
    if sender == UnitName("player") then
        GHC_Core:Debug("Ignore own data")
        return
    end

    if not GHC_Core.Guildspace then return end

    -- TODO Index into pendingTransactions table with sender's name
    self.pendingTransaction = {
        Money = money,
    }
    GHC.UI.GuildBankParentFrame.SendBankDataButton:Disable()
end


function GHC_Bank:ParseGuildBankData(dataString, sender)
    if sender == UnitName("player") then
        GHC_Core:Debug("Ignore own data")
        return
    end

    if not self.pendingTransaction then
        GHC_Core:Printf("Ignoring data from %s without pending transaction", sender)
        return
    end

    if not dataString then
        GHC_Core:Print("Unable to parse nil data from", sender)
        return
    end

    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    local success, tbl, timestamp = GHC_Core:Deserialize(dataString)
    if success then
        -- Timestamp: Ignore data without timestamp or with older/same timestamp
        -- GHC_Core:Debug("Timestamp", timestamp, guildspace.BankTimestamp)
        timestamp = tonumber(timestamp)
        if not timestamp then
            -- TODO Until update-me message is reworked, this is a hack -- alert once per sender per login
            GHC_Core.senders = GHC_Core.senders or {}
            if not GHC_Core.senders[sender] then
                GHC_Core.senders[sender] = true
                GHC_Core:Printf(
                    "Unable to parse data from %s without a valid timestamp - do they have an old version?",
                    sender
                )
            end
            return
        end
        -- Convert guildspace.BankTimestamp to number every time since old versions incorrectly stored it as a string
        if guildspace.BankTimestamp and timestamp <= tonumber(guildspace.BankTimestamp) then
            GHC_Core:Printf("Ignoring old data from %s with timestamp %d", sender, timestamp)
            return
        end

        guildspace.Money = self.pendingTransaction.Money

        -- Wipe old bank data
        guildspace.Bank = {}

        for _, item in ipairs(tbl) do
            -- Type potentially used for sorting
            item.Type = select(6, GetItemInfo(item.ID))
            -- GHC_Core:Debug(i, item.ID, item.Count, item.Type)
            tinsert(guildspace.Bank, item)
        end

        guildspace.BankTimestamp = timestamp

        GHC_Core:Printf("Received Guild Bank data from %s (%d items)", sender, #tbl)

        self:UpdateGuildBankGridView(1)
    else
        GHC_Core:Print("error deserializing:", tbl)
        GHC_Core:Print("Make sure both yours and the Guild Bank's", GHC.addon_name, "is up to date!")
    end
end


--- Close guild bank parsing
function GHC_Bank:GuildBankFinish(message, sender)
    if sender == UnitName("player") then
        GHC_Core:Debug("Ignore own data")
        return
    end

    self.pendingTransaction = nil

    if self.sendGuildBankDataResetTimer then
        GHC_Core:CancelTimer(self.sendGuildBankDataResetTimer)
        self.sendGuildBankDataResetTimer = nil
    end

    GHC.UI.GuildBankParentFrame.SendBankDataButton:Enable()
end


function GHC_Bank:UpdateGuildBankGridView(value)
    -- GHC_Core:Debug("UpdateGuildBankGridView", value)
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(guildspace.Money or 0))

    GHC.UI.GuildBankParentFrame.Timestamp:SetText(guildspace.BankTimestamp)
    if guildspace.BankTimestamp then
        if not self.sendGuildBankDataResetTimer then
            GHC.UI.GuildBankParentFrame.SendBankDataButton:Enable()
        end
    else
        GHC.UI.GuildBankParentFrame.SendBankDataButton:Disable()
    end

    if guildspace.Bank then
        -- table.sort(guildspace.Bank, function(a, b) return a.Type < b.Type end)

        -- Each row has at most 10 items, there are 11 total rows -- prevent overscroll
        local numRowsWithItems = math.ceil(#guildspace.Bank / 10)
        GHC.UI.GuildBankScrollBar:SetMinMaxValues(1, math.max(numRowsWithItems - 10, 1))
        -- GHC_Core:Print(#guildspace.Bank, numRowsWithItems)

        -- Display the right portion of the bank based on scroll position
        local lBound = (math.ceil(value) - 1) * 10 + 1
        local uBound = math.min(lBound + 109, #guildspace.Bank)
        -- GHC_Core:Print(lBound, uBound)
        local c = 1
        for i = lBound, uBound do
            local item = guildspace.Bank[i]
            local gridViewItem = GHC.UI.GuildBankGridViewItems[c]
            gridViewItem.Texture:SetTexture(GetItemIcon(item.ID))
            gridViewItem.Text:SetText(item.Count)
            gridViewItem.Item, gridViewItem.Link = GetItemInfo(item.ID)
            GHC.UI.GuildBankGridViewItems[c]:Show()
            c = c + 1
        end

        -- Reset the remaining guild bank grid view
        for i = c, 110 do
            GHC.UI.GuildBankGridViewItems[i]:Hide()
        end
    end
end


function GHC_Bank:ShowGuildBankTooltip(frame, text)
    -- TODO Evaluate locations to check IsForbidden
    -- if GameTooltip.IsForbidden and GameTooltip:IsForbidden() then return end
    GameTooltip:SetOwner(frame) --, "ANCHOR_CURSOR")
    GameTooltip:SetPoint("TOPLEFT")
    if frame.Link then
        GameTooltip:SetHyperlink(frame.Link)
        GameTooltip:Show()
    end
end


-- TODO This feature can't be used until the Bank inventory is stored uniquely from the alt's inventory
-- because Bank data can only be scanned while the Bank frame is open
function GHC_Bank:UpdateBags(...)
    -- GHC_Core:Debug(IsBankOpen(), self.updateBagTimerId, ...)

    if IsBankOpen() then return end

    -- Wait until it's been calm for at least 5 seconds (NewUpdateBagTimer)
    if self.updateBagTimerId then
        if GHC_Core:TimeLeft(self.updateBagTimerId) > 0 then
            -- Renew the timer if it hasn't expired yet
            self:CancelUpdateBagTimer()
            self:NewUpdateBagTimer()
        else
            -- Send bucketized bag updates while the Bank Frame is not open
            self:CancelUpdateBagTimer()
            self:ScanGuildBank()
        end
    else
        self:NewUpdateBagTimer()
    end
end


function GHC_Bank:CancelUpdateBagTimer()
    if self.updateBagTimerId then
        GHC_Core:CancelTimer(self.updateBagTimerId)
    end
    self.updateBagTimerId = nil
end


function GHC_Bank:NewUpdateBagTimer()
    self.updateBagTimerId = GHC_Core:ScheduleTimer("UpdateBags", 5)
end


function GHC_Bank:IsPlayerEligibleGuildBank(guid)
    local isEligible = false

    local _, rankName, rankIndex = GetGuildInfo("player")  -- Returns 0-indexed rank (0 = GM)
    if rankIndex and rankIndex < 2 then
        isEligible = true
    elseif rankName == "Guild Bank" then
        isEligible = true
    else
        guid = guid or UnitGUID("player")
        local playerName = select(6, GetPlayerInfoByGUID(guid))
        local info = GetGuildInfoText()
        for _, line in ipairs({strsplit("\n", info)}) do
            local first, last = line:find("GHC:")
            if first then
                local args = line:sub(last + 1)
                for name in args:gmatch("^(%a+)^") do
                    if name == playerName then
                        isEligible = true
                        break
                    end
                end
            end
        end
        if isEligible then
            GHC_Core:Print("NOTE: This character has been identified as a Guild Bank alt in the Guild Information window")
        end
    end

    return isEligible
end

