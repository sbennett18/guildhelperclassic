--[==[

Copyright ©2019 Samuel Thomas Pain

The contents of this addon, excluding third-party resources, are
copyrighted to their authors with all rights reserved.

This addon is free to use and the authors hereby grants you the following rights:

1. 	You may make modifications to this addon for private use only, you
    may not publicize any portion of this addon.

2. 	Do not modify the name of this addon, including the addon folders.

3. 	This copyright notice shall be included in all copies or substantial
    portions of the Software.

All rights not explicitly addressed in this license are reserved by
the copyright holders.

]==]--


local ADDON_NAME, GHC = ...


GHC.UI = {
    GuildRosterListViewItemFontSizeLarge = 14.0,
    GuildRosterListViewItemFontSizeSmall = 11.0,
    GuildRosterProfessionIconSize = 17.0,
    ClassSummaryBarHeight = 25.0,
    ClassSummaryBarWidth = 150.0,
    GuildRosterListViewItemFontTooltip = 11.0,
    GuildBankGridviewIconSize = 36,
    GuildBankGridviewIconTextFontSize = 10
}

--this is now just an event frame
GHC.UI.ParentFrame = CreateFrame("FRAME", "GHC_ParentFrame", UIParent)
--[==[
GHC.UI.ParentFrame:SetPoint("TOPLEFT", 350, -12)
GHC.UI.ParentFrame:SetBackdrop({ edgeFile = "interface/dialogframe/ui-dialogbox-border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.ParentFrame:SetSize(975, 500)
GHC.UI.ParentFrame.Texture = GHC.UI.ParentFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
GHC.UI.ParentFrame.Texture:SetTexture("interface/dialogframe/ui-dialogbox-background")
GHC.UI.ParentFrame.Texture:SetPoint('TOPLEFT', 4, -4)
GHC.UI.ParentFrame.Texture:SetPoint('BOTTOMRIGHT', -4, 4)
GHC.UI.ParentFrame:Hide()
]==]--


-------------------------------------------------------------------------------
-- GuildRosterScrollFrame
-------------------------------------------------------------------------------

GHC.UI.RosterScrollFrame = CreateFrame("SCROLLFRAME", "GHC_GuildRosterScrollframe", GuildHelperClassic_RosterFrame)
GHC.UI.RosterScrollFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.RosterScrollFrame:SetSize(400, 405)
GHC.UI.RosterScrollFrame:EnableMouseWheel(true)
GHC.UI.RosterScrollFrame:SetScript("OnMouseWheel", function(self, delta) GHC.Functions.ScrollByAmount(GHC.UI.RosterScrollBar, delta) end)

GHC.UI.RosterScrollFrame.ShowOfflineCheckButton = CreateFrame("CHECKBUTTON", "GHC_GuildRosterScrollFrame_ShowOfflineCheckButton", GHC.UI.RosterScrollFrame, "ChatConfigCheckButtonTemplate")
GHC.UI.RosterScrollFrame.ShowOfflineCheckButton:SetPoint("TOPRIGHT", -96, 42)
GHC.UI.RosterScrollFrame.ShowOfflineCheckButton:SetSize(20, 20)
GHC.UI.RosterScrollFrame.ShowOfflineCheckButton:SetScript("OnClick", function(self)
    if self:GetChecked() then
        PlaySound(SOUNDKIT.IG_MAINMENU_OPTION_CHECKBOX_OFF)
    else
        PlaySound(SOUNDKIT.IG_MAINMENU_OPTION_CHECKBOX_ON)
    end
    SetGuildRosterShowOffline(self:GetChecked())
    GHC_Roster.isRosterFrameDisplayDirty = true
    GHC_Roster:ScanGuildRoster()
end)
GHC.UI.RosterScrollFrame.ShowOfflineCheckButton:SetScript("OnShow", function(self)
    self:SetChecked(GetGuildRosterShowOffline())
end)
_G[GHC.UI.RosterScrollFrame.ShowOfflineCheckButton:GetName() .. "Text"]:SetText("Show Offline")

GHC.UI.RosterScrollFrame.SortNameButton = CreateFrame("BUTTON", "GHC_SortNameButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortNameButton:SetPoint("TOPLEFT", 64, 22)
GHC.UI.RosterScrollFrame.SortNameButton:SetText("Name")
GHC.UI.RosterScrollFrame.SortNameButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortNameButton:SetScript("OnClick", function()
    GHC_Roster.isRosterFrameDisplayDirty = true
    GHC_Roster.rosterSort = "Name"
    GHC_Roster:ScanGuildRoster()
end)

--[==[
GHC.UI.RosterScrollFrame.SortRoleButton = CreateFrame("BUTTON", "GHC_SortRoleButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortRoleButton:SetPoint("TOPLEFT", 114, 22)
GHC.UI.RosterScrollFrame.SortRoleButton:SetText("Role")
GHC.UI.RosterScrollFrame.SortRoleButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortRoleButton:SetScript("OnClick", function() GHC_Roster.rosterSort = 'Role' GHC_Roster:ScanGuildRoster() end)
]==]--

GHC.UI.RosterScrollFrame.SortLevelButton = CreateFrame("BUTTON", "GHC_SortLevelButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortLevelButton:SetPoint("TOPLEFT", 290, 22)
GHC.UI.RosterScrollFrame.SortLevelButton:SetText("Level")
GHC.UI.RosterScrollFrame.SortLevelButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortLevelButton:SetScript("OnClick", function()
    GHC_Roster.isRosterFrameDisplayDirty = true
    GHC_Roster.rosterSort = "Level"
    GHC_Roster:ScanGuildRoster()
end)

GHC.UI.RosterScrollFrame.SortClassButton = CreateFrame("BUTTON", "GHC_SortClassButton", GHC.UI.RosterScrollFrame, "UIPanelButtonTemplate")
GHC.UI.RosterScrollFrame.SortClassButton:SetPoint("TOPLEFT", 340, 22)
GHC.UI.RosterScrollFrame.SortClassButton:SetText("Class")
GHC.UI.RosterScrollFrame.SortClassButton:SetSize(50, 22)
GHC.UI.RosterScrollFrame.SortClassButton:SetScript("OnClick", function()
    GHC_Roster.isRosterFrameDisplayDirty = true
    GHC_Roster.rosterSort = "Class"
    GHC_Roster:ScanGuildRoster()
end)

GHC.UI.RosterScrollFrame.FilterRoleDropDown = CreateFrame("FRAME", "GHC_PlayerRoleDropdown", GHC.UI.RosterScrollFrame, "UIDropDownMenuTemplate")
GHC.UI.RosterScrollFrame.FilterRoleDropDown:SetPoint("TOPLEFT", 100, 26)
GHC.UI.RosterScrollFrame.FilterRoleDropDown.displayMode = nil --"MENU"
UIDropDownMenu_SetWidth(GHC.UI.RosterScrollFrame.FilterRoleDropDown, 50)
UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterRoleDropDown, "Role")
function GHC.UI.RosterScrollFrame.FilterRoleDropDown_Init()
	UIDropDownMenu_Initialize(GHC.UI.RosterScrollFrame.FilterRoleDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		if (level or 1) == 1 then
			for k, role in ipairs(GHC.Db.Roles) do
                info.text = role.Text
                info.arg1 = role.Id
                info.arg2 = nil
                info.func = function() GHC_Roster:FilterByRole(role.Id) UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterRoleDropDown, role.Text) end
                info.isNotRadio = true
                --info.hasArrow = true
                --info.menuList = class.MenuList
                UIDropDownMenu_AddButton(info)
			end
		end
	end)
end

GHC.UI.RosterScrollFrame.FilterProfDropDown = CreateFrame("FRAME", "GHC_PlayerRoleDropdown", GHC.UI.RosterScrollFrame, "UIDropDownMenuTemplate")
GHC.UI.RosterScrollFrame.FilterProfDropDown:SetPoint("TOPLEFT", 170, 26)
GHC.UI.RosterScrollFrame.FilterProfDropDown.displayMode = nil --"MENU"
UIDropDownMenu_SetWidth(GHC.UI.RosterScrollFrame.FilterProfDropDown, 80)
UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterProfDropDown, "Profession")
function GHC.UI.RosterScrollFrame.FilterProfDropDown_Init()
	UIDropDownMenu_Initialize(GHC.UI.RosterScrollFrame.FilterProfDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
		if (level or 1) == 1 then
			for k, prof in ipairs(GHC.Db.Professions) do
                info.text = prof.Name
                info.arg1 = prof.Id
                info.arg2 = nil
                -- TODO use self?
                info.func = function() GHC_Roster:FilterByProfession(prof.Name); UIDropDownMenu_SetText(GHC.UI.RosterScrollFrame.FilterProfDropDown, prof.Name) end
                info.isNotRadio = true
                --info.hasArrow = true
                --info.menuList = class.MenuList
                UIDropDownMenu_AddButton(info)
			end
		end
	end)
end

--[==[
GHC.UI.RosterScrollFrame.Header = GHC.UI.RosterScrollFrame:CreateFontString("GHC_PlayerOptionsFrame_Header", 'OVERLAY', 'GameFontNormal')
GHC.UI.RosterScrollFrame.Header:SetPoint("TOP", 0, 16)
GHC.UI.RosterScrollFrame.Header:SetText("Members")
GHC.UI.RosterScrollFrame.Header:SetTextColor(1,1,1,1)
GHC.UI.RosterScrollFrame.Header:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))
]==]--

GHC.UI.RosterScrollBar = CreateFrame("Slider", "AJ_ConsumablesListViewFrame_ScrollFrame", GHC.UI.RosterScrollFrame, "UIPanelScrollBarTemplate")
GHC.UI.RosterScrollBar:SetPoint("TOPLEFT", GHC.UI.RosterScrollFrame, "TOPRIGHT", -16, -22)
GHC.UI.RosterScrollBar:SetPoint("BOTTOMLEFT", GHC.UI.RosterScrollFrame, "BOTTOMRIGHT", -16, 8)
GHC.UI.RosterScrollBar:SetMinMaxValues(1, 10)
GHC.UI.RosterScrollBar:SetValueStep(1)
GHC.UI.RosterScrollBar.scrollStep = 1  -- Required for scroll up/down slider buttons
GHC.UI.RosterScrollBar:SetStepsPerPage(9)
GHC.UI.RosterScrollBar:SetObeyStepOnDrag(true)
GHC.UI.RosterScrollBar:SetValue(1)
GHC.UI.RosterScrollBar:SetWidth(16)
GHC.UI.RosterScrollBar:SetScript("OnValueChanged", function(self, value) GHC_Roster:GuildRosterScrollBarChanged(value) end)

GHC.UI.GuildRosterListViewItems = {}
function GHC.UI.DrawGuildRosterListview()
    for i = 1, 10 do
        local f = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i), GHC.UI.RosterScrollFrame)
        f:SetPoint("TOPLEFT", 12, ((41 * (i - 1)) * -1) - 2)
        f:SetSize(370, 41)
        f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", insets = { left = 1, right = 1, top = 1, bottom = 1 } })
        f:SetBackdropColor(0, 0, 0, 1.0)
        f:EnableMouse(true)
        f:SetScript(
            "OnMouseUp",
            function(self, mouseButton)
                if mouseButton == "RightButton" then
                    local menu = {
                        -- gsub example: "Thrall [Cairne]" --> "Thrall"
                        {
                            text = "Whisper",
                            func = function()
                                _G.ChatFrame_SendTell(f.Name:GetText():gsub("( [[].-[]])", ""))
                            end
                        },
                        -- { text = "Send Mail", func = function() print("Send Mail") end },
                    }
                    local menuFrame = CreateFrame("FRAME", "ExampleMenuFrame", f, "UIDropDownMenuTemplate")
                    -- menuFrame:SetPoint("CENTER", f, "CENTER")
                    -- EasyMenu(menu, menuFrame, menuFrame, 0, 0, "MENU")
                    EasyMenu(menu, menuFrame, "cursor", 0, 0, "MENU")
                end
            end
        )

        f.RaceIcon = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i.."_RaceIcon"), f)
        f.RaceIcon:SetPoint("LEFT", 6, 0)
        f.RaceIcon:SetSize(32, 32)
        f.RaceIcon.Texture = f.RaceIcon:CreateTexture("$parent_Background", "BACKGROUND")
        f.RaceIcon.Texture:SetAllPoints(f.RaceIcon)
        f.RaceIcon:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.RoleText = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_RoleText"), "OVERLAY", "GameFontDisable")
        f.RoleText:SetPoint("TOPLEFT", 54, -24)

        f.RoleIconTank = CreateFrame("FRAME", tostring("GHC_GuildRosterLiewViewItem"..i.."_RoleIconTank"), f)
        f.RoleIconTank:SetPoint("BOTTOMLEFT", 54, 2)
        f.RoleIconTank:SetSize(19, 19)
        f.RoleIconTank.Texture = f.RoleIconTank:CreateTexture("$parent_Background", "BACKGROUND")
        f.RoleIconTank.Texture:SetAllPoints(f.RoleIconTank)
        f.RoleIconTank.Texture:SetTexture("Interface/LFGFrame/UI-LFG-ICON-PORTRAITROLES")
        f.RoleIconTank.Texture:SetTexCoord(0, 0.296875, 0.34375, 0.640625)
        f.RoleIconTank:SetScript("OnEnter", function(self) if self:GetAlpha() == 1.00 then GHC.Functions.ShowTooltip(self, "Tank") end end)
        f.RoleIconTank:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.RoleIconHealer = CreateFrame("FRAME", tostring("GHC_GuildRosterLiewViewItem"..i.."_RoleIconHealer"), f)
        f.RoleIconHealer:SetPoint("LEFT", f.RoleIconTank, "RIGHT", 2, 0)
        f.RoleIconHealer:SetSize(19, 19)
        f.RoleIconHealer.Texture = f.RoleIconHealer:CreateTexture("$parent_Background", "BACKGROUND")
        f.RoleIconHealer.Texture:SetAllPoints(f.RoleIconHealer)
        f.RoleIconHealer.Texture:SetTexture("Interface/LFGFrame/UI-LFG-ICON-PORTRAITROLES")
        f.RoleIconHealer.Texture:SetTexCoord(0.3125, 0.609375, 0.015625, 0.3125)
        f.RoleIconHealer:SetScript("OnEnter", function(self) if self:GetAlpha() == 1.00 then GHC.Functions.ShowTooltip(self, "Healer") end end)
        f.RoleIconHealer:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.RoleIconDamage = CreateFrame("FRAME", tostring("GHC_GuildRosterLiewViewItem"..i.."_RoleIconDamage"), f)
        f.RoleIconDamage:SetPoint("LEFT", f.RoleIconHealer, "RIGHT", 2, 0)
        f.RoleIconDamage:SetSize(19, 19)
        f.RoleIconDamage.Texture = f.RoleIconDamage:CreateTexture("$parent_Background", "BACKGROUND")
        f.RoleIconDamage.Texture:SetAllPoints(f.RoleIconDamage)
        f.RoleIconDamage.Texture:SetTexture("Interface/LFGFrame/UI-LFG-ICON-PORTRAITROLES")
        f.RoleIconDamage.Texture:SetTexCoord(0.3125, 0.609375, 0.34375, 0.640625)
        f.RoleIconDamage:SetScript("OnEnter", function(self) if self:GetAlpha() == 1.00 then GHC.Functions.ShowTooltip(self, "DPS") end end)
        f.RoleIconDamage:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.ClassIcon = CreateFrame("FRAME", tostring("GHC_GuildRosterListViewItem"..i.."_RaceIcon"), f)
        f.ClassIcon:SetPoint("RIGHT", -2, 0)
        f.ClassIcon:SetSize(40, 40)
        f.ClassIcon.Texture = f.ClassIcon:CreateTexture("$parent_Background", "BACKGROUND")
        f.ClassIcon.Texture:SetAllPoints(f.ClassIcon)
        f.ClassIcon:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.Name = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_NameText"), "OVERLAY", "GameFontNormalLarge")
        f.Name:SetPoint("TOPLEFT", 52, -2)

        f.Level = f:CreateFontString(tostring("GHC_GuildRosterListViewItem"..i.."_LevelText"), "OVERLAY", "GameFontNormal")
        f.Level:SetPoint("RIGHT", -52, 0)
        f.Level:SetTextColor(1,1,1,1)
        f.Level:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge + 3))

        f.Professions = {}
        f.Professions[1] = {}
        f.Professions[1].Icon = CreateFrame("FRAME", tostring("GHC_GuildMemberDetailFrame_Profession1Icon"), f)
        f.Professions[1].Icon:SetPoint("BOTTOMRIGHT", -90, 3)
        f.Professions[1].Icon:SetSize(tonumber(GHC.UI.GuildRosterProfessionIconSize), tonumber(GHC.UI.GuildRosterProfessionIconSize))
        f.Professions[1].Icon.Texture = f.Professions[1].Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Professions[1].Icon.Texture:SetAllPoints(f.Professions[1].Icon)
        f.Professions[1].Text = nil
        f.Professions[1].Icon:SetScript("OnEnter", function(self) GHC.Functions.ShowTooltip(self, f.Professions[1].Text) end)
        f.Professions[1].Icon:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.Professions[2] = {}
        f.Professions[2].Icon = CreateFrame("FRAME", tostring("GHC_GuildMemberDetailFrame_Profession2Icon"), f)
        f.Professions[2].Icon:SetPoint("BOTTOMRIGHT", -120, 3)
        f.Professions[2].Icon:SetSize(tonumber(GHC.UI.GuildRosterProfessionIconSize), tonumber(GHC.UI.GuildRosterProfessionIconSize))
        f.Professions[2].Icon.Texture = f.Professions[2].Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Professions[2].Icon.Texture:SetAllPoints(f.Professions[2].Icon)
        f.Professions[2].Text = nil
        f.Professions[2].Icon:SetScript("OnEnter", function(self) GHC.Functions.ShowTooltip(self, f.Professions[2].Text) end)
        f.Professions[2].Icon:SetScript("OnLeave", function(self) GHC.UI.Tooltip:Hide() end)

        f.PublicNote = nil
        --f.RaceIcon:SetScript("OnEnter", function(self) GHC.Functions.ShowTooltip(f.RaceIcon, f.PublicNote) end)
        --f.RaceIcon:SetScript("OnLeave", function() GHC.UI.Tooltip:Hide() end)

        f:Hide()
        GHC.UI.GuildRosterListViewItems[i] = f
    end
end


-------------------------------------------------------------------------------
-- GuildRosterSummaryFrame
-------------------------------------------------------------------------------

GHC.UI.GuildSummaryFrame = CreateFrame("FRAME", "GHC_GuildSummaryFrame", GuildHelperClassic_SummaryFrame)
--GHC.UI.GuildSummaryFrame:SetBackdrop({ edgeFile = "Interface/Tooltips/UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.GuildSummaryFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.GuildSummaryFrame:SetSize(400, 405)

GHC.UI.GuildSummaryFrameTitle = GHC.UI.GuildSummaryFrame:CreateFontString("GHC_GuildSummaryTitle", 'OVERLAY', 'GameFontNormal')
GHC.UI.GuildSummaryFrameTitle:SetPoint("TOP", 0, -8)
GHC.UI.GuildSummaryFrameTitle:SetText("Class Summary")
GHC.UI.GuildSummaryFrameTitle:SetTextColor(1,1,1,1)
GHC.UI.GuildSummaryFrameTitle:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))

GHC.UI.ClassSummaryBars = {}
function GHC.UI.DrawGuildClassSummaryBars()
    for k, v in ipairs(GHC.Db.ClassIDs) do
        local f = CreateFrame("FRAME", tostring("GHC_GuildSummaryClassFrame"..k), GHC.UI.GuildSummaryFrame)
        f:SetSize(260, 44)
        f:SetPoint("TOPLEFT", 8, (k * GHC.UI.ClassSummaryBarHeight) * -1)

        f.Icon = CreateFrame("FRAME", tostring("GHC_GuildSummaryClassFrame_Icon"..k), f)
        f.Icon:SetPoint("LEFT", 4, 0)
        f.Icon:SetSize(GHC.UI.ClassSummaryBarHeight, GHC.UI.ClassSummaryBarHeight)
        f.Icon.Texture = f.Icon:CreateTexture("$parent_Background", "BACKGROUND")
        f.Icon.Texture:SetAllPoints(f.Icon)
        f.Icon.Texture:SetTexture(tostring("Interface/Addons/GuildHelperClassic/ClassIcons/"..v))

        f.Text = f:CreateFontString(tostring("GHC_GuildSummaryClassFrame_Name"..k), 'OVERLAY', 'GameFontNormal')
        f.Text:SetPoint("RIGHT", 0, 0)
        f.Text:SetTextColor(1,1,1,1)
        f.Text:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeSmall))
        f.Text:SetText(v)

        f.StatusBar = CreateFrame("StatusBar", tostring("GHC_GuildSummaryClassFrame_StatusBar"..k), f)
        f.StatusBar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
        f.StatusBar:GetStatusBarTexture():SetHorizTile(false)
        f.StatusBar:SetMinMaxValues(0, 100)
        f.StatusBar:SetSize(GHC.UI.ClassSummaryBarWidth, GHC.UI.ClassSummaryBarHeight * 0.9)
        f.StatusBar:SetPoint('LEFT', 40, 0)
        f.StatusBar:SetStatusBarColor(GHC.Db.ClassColours[v].r, GHC.Db.ClassColours[v].g, GHC.Db.ClassColours[v].b)

        GHC.UI.ClassSummaryBars[k] = f
    end
end


-------------------------------------------------------------------------------
-- PlayerOptionsFrame
-------------------------------------------------------------------------------

GHC.UI.PlayerOptionsFrame = CreateFrame("FRAME", "GHC_PlayerOptionsFrame", GuildHelperClassic_OptionsFrame)
--GHC.UI.PlayerOptionsFrame:SetBackdrop({ edgeFile = "Interface/Tooltips/UI-Tooltip-Border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
GHC.UI.PlayerOptionsFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.PlayerOptionsFrame:SetSize(400, 405)

GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton = CreateFrame("BUTTON", "GHC_UpdateAltInfoButton", GHC.UI.PlayerOptionsFrame, "UIPanelButtonTemplate")
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetPoint("CENTER", 0, 0)
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetText("Configure")
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetSize(80, 22)
-- Standard workaround call OpenToCategory twice https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
GHC.UI.PlayerOptionsFrame.UpdateAltInfoButton:SetScript("OnClick", function() InterfaceOptionsFrame_OpenToCategory(GHC.addon_name); InterfaceOptionsFrame_OpenToCategory(GHC.addon_name) end)


-------------------------------------------------------------------------------
-- GuildBankParentFrame
-------------------------------------------------------------------------------

GHC.UI.GuildBankParentFrame = CreateFrame("SCROLLFRAME", "GHC_GuildBankParentFrame", GuildHelperClassic_BankFrame) --, "TooltipBorderedFrameTemplate")
GHC.UI.GuildBankParentFrame:SetPoint("TOPLEFT", 16, -86)
GHC.UI.GuildBankParentFrame:SetSize(400, 405)
GHC.UI.GuildBankParentFrame:EnableMouseWheel(true)
GHC.UI.GuildBankParentFrame:SetScript("OnMouseWheel", function(self, delta)
    GHC.Functions.ScrollByAmount(GHC.UI.GuildBankScrollBar, delta)
end)

GHC.UI.GuildBankParentFrame.SendBankDataButton = CreateFrame("BUTTON", "GHC_GuildBankParentFrame_SendBankDataButton", GHC.UI.GuildBankParentFrame, "UIPanelButtonTemplate")
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetPoint("TOPLEFT", 64, 22)
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetSize(80, 22)
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetText("Share Data")
GHC.UI.GuildBankParentFrame.SendBankDataButton:SetScript("OnClick", function(self) GHC_Bank:SendGuildBankData() end)

GHC.UI.GuildBankParentFrame.Timestamp = GHC.UI.GuildBankParentFrame:CreateFontString("GHC_GuildBankParentFrame_Timestamp", "OVERLAY", "GameFontDisableSmall")
GHC.UI.GuildBankParentFrame.Timestamp:SetPoint("BOTTOMRIGHT", GHC.UI.GuildBankParentFrame.SendBankDataButton, "TOPRIGHT", 0, 6)
GHC.UI.GuildBankParentFrame.Timestamp:SetText("")

GHC.UI.GuildBankParentFrame.MoneyText = GHC.UI.GuildBankParentFrame:CreateFontString("GHC_GuildBankParentFrame_MoneyText", "OVERLAY", "GameFontNormal")
GHC.UI.GuildBankParentFrame.MoneyText:SetPoint("TOPRIGHT", 0, 16)
GHC.UI.GuildBankParentFrame.MoneyText:SetText(GetCoinTextureString(0))
GHC.UI.GuildBankParentFrame.MoneyText:SetTextColor(1, 1, 1, 1)
--GHC.UI.GuildBankParentFrame.MoneyText:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontSizeLarge))

GHC.UI.GuildBankScrollBar = CreateFrame("Slider", "GHC_GuildBankParentFrame_ScrollFrame", GHC.UI.GuildBankParentFrame, "UIPanelScrollBarTemplate")
GHC.UI.GuildBankScrollBar:SetPoint("TOPLEFT", GHC.UI.GuildBankParentFrame, "TOPRIGHT", -16, -22)
GHC.UI.GuildBankScrollBar:SetPoint("BOTTOMLEFT", GHC.UI.GuildBankParentFrame, "BOTTOMRIGHT", -16, 8)
GHC.UI.GuildBankScrollBar:SetMinMaxValues(1, 1)
GHC.UI.GuildBankScrollBar:SetValueStep(1.0)
GHC.UI.GuildBankScrollBar.scrollStep = 1
GHC.UI.GuildBankScrollBar:SetValue(1)
GHC.UI.GuildBankScrollBar:SetWidth(16)
GHC.UI.GuildBankScrollBar:SetScript("OnValueChanged", function(self, value) GHC_Bank:UpdateGuildBankGridView(value) end)

GHC.UI.GuildBankGridViewItems = {}
function GHC.UI.DrawGuildBankGridView()
    for row = 1, 11 do
        for col = 1, 10 do
            local f = CreateFrame("FRAME", tostring("GHC_GuildBankGridViewItems_"..row..'_'..col), GHC.UI.GuildBankParentFrame)
            f:SetSize(GHC.UI.GuildBankGridviewIconSize, GHC.UI.GuildBankGridviewIconSize)
            f:SetPoint("TOPLEFT", ((col - 1) * GHC.UI.GuildBankGridviewIconSize + 5 ) + 10, (((row - 1) * GHC.UI.GuildBankGridviewIconSize) + 5) * -1 )

            f.Texture = f:CreateTexture("$parent_Background", "BACKGROUND")
            f.Texture:SetAllPoints(f)

            f.Text = f:CreateFontString("$parent_Text", "OVERLAY", "GameFontNormal")
            f.Text:SetPoint("BOTTOMRIGHT", -4, 4)
            f.Text:SetTextColor(1,1,1,1)
            f.Text:SetFont("Fonts\\FRIZQT__.TTF", GHC.UI.GuildBankGridviewIconTextFontSize)

            f.Item = nil
            f.Link = nil

            f:SetScript("OnEnter", function() GHC_Bank:ShowGuildBankTooltip(f, f.Item) end)
            f:SetScript("OnLeave", function() GameTooltip:Hide() GameTooltip_SetDefaultAnchor(GameTooltip, UIParent) end) -- GHC.UI.GuildBankTooltip:Hide() end)

            f:Hide()

            table.insert(GHC.UI.GuildBankGridViewItems, f)
        end
    end
end


-------------------------------------------------------------------------------
-- StaticPopupDialogs
-------------------------------------------------------------------------------

StaticPopupDialogs["GHC_UpdatePopup"] = {
	text = "There is an update available for Guild Helper Classic!", button1 = "Ok",
	timeout = 0, whileDead = true, hideOnEscape = true,	preferredIndex = 3,
}

StaticPopupDialogs["GHC_FixMe"] = {
	text = "To fix any errors you can reset the addon settings, you will need to provide your guild name. |cffC41F3BThis is wipe all data currently stored!|r The name must be an exact match including upper/lower case letters.", button1 = "Ok", button2 = "Cancel",
    hasEditBox = true, EditBoxOnTextChanged = function(self) if self:GetText() ~= '' then self:GetParent().button1:Enable() else self:GetParent().button1:Disable() end end,
	OnShow = function(self) self.editBox:SetText('Guild Name Required!') self.button1:Disable() end,
	OnAccept = function(self) GHC.Functions.Reset(tostring(self.editBox:GetText())) end,
	timeout = 0, whileDead = true, hideOnEscape = true,	preferredIndex = 3,
}


-------------------------------------------------------------------------------
-- Tooltips
-------------------------------------------------------------------------------

--used for roster listview profession icons
GHC.UI.Tooltip = CreateFrame("FRAME", "GHC_Tooltip", UIParent, "TooltipBorderedFrameTemplate")
GHC.UI.Tooltip.Text = GHC.UI.Tooltip:CreateFontString("GHC_TooltipText", "OVERLAY", "GameFontNormal")
GHC.UI.Tooltip.Text:SetPoint("CENTER", 0, 0)
GHC.UI.Tooltip.Text:SetFont("Fonts\\FRIZQT__.TTF", tonumber(GHC.UI.GuildRosterListViewItemFontTooltip))
--GHC.UI.Tooltip.Text:SetTextColor(1,1,1,1)


-------------------------------------------------------------------------------
-- MinimapIcon
-------------------------------------------------------------------------------

do
    -- TODO Add ability to show/hide the minimap button from config
    local ldb = LibStub("LibDataBroker-1.1")
    GHC.UI.MinimapButton = ldb:NewDataObject(GHC.addon_name, {
        type = "data source",
        icon = "Interface\\AddOns\\GuildHelperClassic\\GuildHelperClassic",
        -- icon = "Interface\\GuildFrame\\GuildLogo-NoLogo",
        -- icon = "Interface\\GuildFrame\\GuildEmblemsLG_01",
        OnClick = function(self, button)
            if button == "RightButton" then
                -- Standard workaround call OpenToCategory twice
                -- https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
                InterfaceOptionsFrame_OpenToCategory(GHC.addon_name)
                InterfaceOptionsFrame_OpenToCategory(GHC.addon_name)
            else
                GHC.UI.Toggle()
            end
        end,
        OnTooltipShow = function(tooltip)
            if not tooltip or not tooltip.AddLine then return end
            tooltip:AddLine(GHC.addon_name)
        end,
    })

    GHC.UI.MinimapIcon = LibStub("LibDBIcon-1.0")
end


function GHC.UI.Toggle()
    if GuildHelperClassic then
        if GuildHelperClassic:IsVisible() then
            GuildHelperClassic:Hide()
        else
            GuildHelperClassic:Show()
        end
    end
end
