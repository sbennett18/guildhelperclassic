local ADDON_NAME, GHC = ...
GHC_Roster = {...}  -- Public interface
-- local Roster = {...}  -- Private interface

local IsMemberOnline = nil


local RoleIconsAlpha = {
    -- { RoleIconDamage = 0.25, RoleIconHealer = 0.25, RoleIconTank = 0.25, },  -- [0] Unknown
    { RoleIconDamage = 1.00, RoleIconHealer = 0.25, RoleIconTank = 0.25, },  -- [1] DPS
    { RoleIconDamage = 0.25, RoleIconHealer = 1.00, RoleIconTank = 0.25, },  -- [2] Healer
    { RoleIconDamage = 0.25, RoleIconHealer = 0.25, RoleIconTank = 1.00, },  -- [3] Tank
    { RoleIconDamage = 1.00, RoleIconHealer = 1.00, RoleIconTank = 0.25, },  -- [4] DPS-Healer
    { RoleIconDamage = 1.00, RoleIconHealer = 0.25, RoleIconTank = 1.00, },  -- [4] DPS-Tank
    { RoleIconDamage = 0.25, RoleIconHealer = 1.00, RoleIconTank = 1.00, },  -- [6] Healer-Tank
    { RoleIconDamage = 1.00, RoleIconHealer = 1.00, RoleIconTank = 1.00, },  -- [7] Any
    -- { RoleIconDamage = 0.00, RoleIconHealer = 0.00, RoleIconTank = 0.00, },  -- [8] Guild Bank
}
local RoleIconsAlphaNone = { RoleIconDamage = 0.00, RoleIconHealer = 0.00, RoleIconTank = 0.00, }


local rosterSortOrders = {
    Name  = {"Name"},
    Class = {"Class", "Level", "Name"},
    Level = {"Level", "Class", "Name"},
    Role  = {"Role", "Name"},
}

local rosterSortDescending = {
    Name  = false,  -- ascending
    Class = false,  -- ascending
    Level = true,   -- descending
    Role  = true,   -- descending
}


local function updateListViewItem(listViewItem, member, isOffline)
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    -- Name, MainsName: set to "-" if nil
    if member.Name then
        local name
        if member.Name == member.MainsName then
            name = member.Name
        else
            name = ("%s [|cffABD473%s|r]"):format(
                member.Name,
                member.MainsName or "-"
            )
        end
        listViewItem.Name:SetText(name)
        listViewItem.Name:SetFontObject(isOffline and "GameFontDisableLarge" or "GameFontNormalLarge")
    else
        listViewItem.Name:SetText("")
    end

    -- Level
    listViewItem.Level:SetText(member.Level or "")

    -- RoleText
    local isGuildBank = member.Role == #GHC.Db.RoleIconTextString
    local roleIcons = RoleIconsAlpha[member.Role]
    if roleIcons then
        listViewItem.RoleText:SetText("")
        for k, alpha in pairs(roleIcons) do
            listViewItem[k]:SetAlpha(alpha)
        end
    else
        for k, alpha in pairs(RoleIconsAlphaNone) do
            listViewItem[k]:SetAlpha(alpha)
        end
        if isGuildBank then
            listViewItem.RoleText:SetFontObject("GameFontWhite")
            listViewItem.RoleText:SetText(GetCoinTextureString(guildspace and guildspace.Money or 0))
        else
            listViewItem.RoleText:SetFontObject("GameFontDisableSmall")
            listViewItem.RoleText:SetText("Unknown Role")
        end
    end

    -- ClassIcon
    local classIconTexture, classIconTooltipFunction
    if isGuildBank then
        classIconTexture = 133785  -- INV_Misc_Coin_02
        classIconTooltipFunction = function(self) GHC.Functions.ShowTooltip(self, "Guild Bank") end
    elseif member.Class then
        classIconTexture = "Interface/Addons/GuildHelperClassic/ClassIcons/" .. member.Class
        classIconTooltipFunction = function(self) GHC.Functions.ShowTooltip(self, member.Class) end
    end
    listViewItem.ClassIcon.Texture:SetTexture(classIconTexture)
    listViewItem.ClassIcon:SetScript("OnEnter", classIconTooltipFunction)

    -- RaceIcon
    local raceIconTooltipFunction
    if member.Gender and member.Race then
        listViewItem.RaceIcon.Texture:SetTexture(GHC.Db.RaceIcons[member.Gender][member.Race])
        raceIconTooltipFunction = function(self) GHC.Functions.ShowTooltip(self, member.Race, member.Gender) end
    else
        listViewItem.RaceIcon.Texture:SetTexture(nil)
    end
    listViewItem.RaceIcon:SetScript("OnEnter", raceIconTooltipFunction)

    local primaries = member.Professions and member.Professions.Primary or {}
    for i, professionFrame in ipairs(listViewItem.Professions) do
        local profession = primaries[i]
        if profession then
            professionFrame.Icon:Show()
            professionFrame.Icon.Texture:SetTexture(GHC.Db.ProfessionIcons[profession.Name])
            professionFrame.Text = ("%s [%d]"):format(profession.Name, profession.Level)
        else
            professionFrame.Icon:Hide()
            professionFrame.Icon.Texture:SetTexture(nil)
            professionFrame.Text = ""
        end
    end

    -- TODO if isOffline then DisableRightClickMenu

    listViewItem:Show()
end


--- Clear roster list view items starting from lBound up to uBound
-- @param[opt=1] lBound Lower bound of listview to clear
-- @param[opt=10] uBound Upper bound of listview to clear
function GHC_Roster:ClearGuildRosterListView(lBound, uBound)
    -- The loop will not excecute if lBound > uBound
    for i = (lBound or 1), (uBound or 10) do
        GHC.UI.GuildRosterListViewItems[i]:Hide()
        -- updateListViewItem(GHC.UI.GuildRosterListViewItems[i], emptyMember)
    end
end


--- Scroll by three members at a time
function GHC_Roster:GuildRosterScrollBarChanged(value, rosterTable)
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    local rosterKeys = GHC_Roster.rosterKeysFiltered or guildspace.RosterKeysSorted

    rosterTable = rosterTable or guildspace.Roster

    local lBound = math.ceil(value)
    local uBound = math.min(lBound + 9, #rosterKeys)
    local i = 1
    -- Use i to access the listview item and k to access the guild roster
    for k = lBound, uBound do
        local guid = rosterKeys[k]
        local member = rosterTable[guid]
        updateListViewItem(GHC.UI.GuildRosterListViewItems[i], member, not IsMemberOnline[guid])
        i = i + 1
    end

    -- Only clear those remaining unchanged items
    GHC_Roster:ClearGuildRosterListView(i)
end


local function RedrawRosterListView(roster, guids)
    -- Update listview scroll limits and scroll to the top
    GHC.UI.RosterScrollBar:SetMinMaxValues(1, math.max(#guids - 9, 1))
    GHC.UI.RosterScrollBar:SetValue(1)

    -- Update at most the first 10 members or all members if there are less than 10
    local c = 1
    for i = 1, math.min(#guids, 10) do
        local guid = guids[i]
        local listViewItem = GHC.UI.GuildRosterListViewItems[c]
        updateListViewItem(listViewItem, roster[guid], not IsMemberOnline[guid])
        c = c + 1
    end

    -- Clear remaining icons & text from listview
    GHC_Roster:ClearGuildRosterListView(c)

    GHC_Roster.isRosterFrameDisplayDirty = false
end

-- TODO Consider making these Filter functions members of the Roster table
-- Guildspace.Roster:FilterByRole(role)
-- Guildspace.Roster:FilterByProfession(role)
-- Guildspace.Roster:FilterBy(filterFunc)

--- Filter the guild roster by role
-- @param role Role to filter
function GHC_Roster:FilterByRole(role)
    GHC_Roster:FilterRosterBy(
        function(member)
            -- TODO Reimplement the Role setting to avoid this mess
            if role == 1 then
                return member.Role == 1 or member.Role == 4 or member.Role == 5 or member.Role == 7
            elseif role == 2 then
                return member.Role == 2 or member.Role == 4 or member.Role == 6 or member.Role == 7
            elseif role == 3 then
                return member.Role == 3 or member.Role == 5 or member.Role == 6 or member.Role == 7
            elseif role == 4 then
                return member.Role == 4 or member.Role == 7
            elseif role == 5 then
                return member.Role == 5 or member.Role == 7
            elseif role == 6 then
                return member.Role == 6 or member.Role == 7
            elseif role == 7 then
                return member.Role ~= 8
            elseif role == 8 then
                return member.Role == 8
            else
                return false
            end
        end
    )
end


--- Filter the guild roster by profession
-- @param prof Profession name to filter
function GHC_Roster:FilterByProfession(prof)
    local function filterFunc(member)
        for _, professions in pairs(member.Professions or {}) do
            for _, profession in ipairs(professions or {}) do
                if profession.Name == prof then
                    return true
                end
            end
        end
        return false
    end
    GHC_Roster:FilterRosterBy(filterFunc)
end


--- Filter the GuildDb roster including only members for which the passed `filterFunc` returns true
-- @param[opt=all] filterFunc(member) Include members for which filterFunc(member) returns true
function GHC_Roster:FilterRosterBy(filterFunc)
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    -- Apply filterFunc to the roster and only display filtered members
    local filteredRosterKeys = {}
    for _, guid in ipairs(guildspace.RosterKeysSorted) do
        if filterFunc(guildspace.Roster[guid]) then
            tinsert(filteredRosterKeys, guid)
        end
    end
    GHC_Roster.rosterKeysFiltered = filteredRosterKeys

    GHC_Roster.isRosterFrameDisplayDirty = true
    RedrawRosterListView(guildspace.Roster, filteredRosterKeys)
end  -- GHC_Roster:FilterRosterBy


--- Scans the guild roster, creates a backup db which is checked against to update player info after roster scan
function GHC_Roster:ScanGuildRoster()
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    -- Make a copy of Roster to RosterBackup
    guildspace.RosterBackup = guildspace.Roster

    -- Wipe guild roster db
    guildspace.Roster = {}
    IsMemberOnline = {}

    local totalMembers, _, _ = GetNumGuildMembers()

    for i = 1, totalMembers do
        -- name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid
        local name, _, _, level, _, _, publicNote, _, isOnline, _, class, _, _, _, _, _, guid = GetGuildRosterInfo(i)
        name = strsplit("-", name, 2)

        local member = {
            Name = name,
            Level = level,
            Class = class,
            PublicNote = publicNote,
        }

        -- local memberBackup = guildspace.RosterBackup[guid]
        -- local memberBackupPrimaries = memberBackup.Professions.Primary
        -- local member = {
        --     Name = name or memberBackup.Name,
        --     Level = level or memberBackup.Level,
        --     Class = class or memberBackup.Class,
        --     Gender = memberBackup.Class,
        --     Race = memberBackup.Race,
        --     Professions = {
        --         Primary = {
        --             {
        --                 Name = memberBackupPrimaries[1].Name,
        --                 Level = memberBackupPrimaries[1].Level,
        --             },
        --             {
        --                 Name = memberBackupPrimaries[2].Name,
        --                 Level = memberBackupPrimaries[2].Level,
        --             },
        --         },
        --     },
        --     Role = memberBackup.Role,
        --     PublicNote = publicNote or memberBackup.PublicNote,
        --     MainsName = memberBackup.MainsName,
        -- }
        -- guildspace.Roster[guid] = member

        IsMemberOnline[guid] = isOnline

        -- If we have a back up use it as a baseline, otherwise just fill as much as we know from member
        -- Default Role to 1 for DPS-only classes and 0 "Unknown" for all others
        guildspace.Roster[guid] = GHC_Core:assign({Role = 0}, guildspace.RosterBackup[guid] or {}, member)
        if guildspace.Roster[guid].Role == 0 then
            if class == "HUNTER" or class == "MAGE" or class == "ROGUE" or class == "WARLOCK" then
                guildspace.Roster[guid].Role = 1
            end
        end
    end

    GHC_Roster.rosterKeysFiltered = nil  -- Unfilter the Roster

    -- Perform multi-level sort, e.g. Class -> Level -> Name
    guildspace.RosterKeysSorted = GHC_Core:getSortedKeysByValue(
        guildspace.Roster,
        function(a, b)
            for _, k in ipairs(rosterSortOrders[GHC_Roster.rosterSort or "Name"]) do
                local descending = rosterSortDescending[k]
                if a[k] > b[k] then return descending end
                if a[k] < b[k] then return not descending end
            end
        end
    )

    if not GetGuildRosterShowOffline() then
        local ni = 1
        local ol = #guildspace.RosterKeysSorted
        for oi, guid in ipairs(guildspace.RosterKeysSorted) do
            if IsMemberOnline[guid] then
                guildspace.RosterKeysSorted[ni] = guid
                ni = ni + 1
            end
        end
        for i = ni, ol do
            guildspace.RosterKeysSorted[i] = nil
        end
    end

    if GHC_Roster.isRosterFrameDisplayDirty then
        RedrawRosterListView(guildspace.Roster, guildspace.RosterKeysSorted)
    end
end