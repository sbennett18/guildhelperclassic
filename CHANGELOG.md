# Changelog

## `0.2`

### `0.2.4` - 11/19/2019

* #8: Add online/offline filtering checkbutton
* #3: Fix: Don't show "[-]" for Main Characters

### `0.2.3` - 11/7/2019

* #7: Fix Professions not registering

### `0.2.2` - 11/6/2019

* #3: Don't show "[-]" for Main Characters
* #3: Print reminder to set Main Character and Role

### `0.2.1` - 11/2/2019

* #4: Fix unable to send guild bank data
* Don't parse data sent from yourself

### `0.2.0` - 10/31/2019

* Another round of significant refactoring, attempt to modularize the code base
* #24: Add ability to authorize Guild Bank alt using Guild Information window
* #24: Add ability to authorize Guild Bank alt using dedicated guild rank "Guild Bank"
* #34: Attempt to fix random inability to scan guild bank
* Delay sending/parsing data until out of combat

## `0.1.0`

**`0.1.0` was a significant rewrite that touched almost every part of the addon.**

### `0.1.7` - 10/30/2019

* #35: Fix embed order to load LibDataBroker before LDBIcon

### `0.1.6` - 10/10/2019

* #27: Attempt to fix game framerate drop/stutter
* #22: Fix inconsistent/incorrect role icons while scrolling
* #16: Add tooltips for role icons, race icon, and class icon
* #30: Fix lua error scrolling Bank frame
** Make Bank frame scroll one row at at time
* #11: Add right-click whisper support to Roster list
* #3/#6: Filtering Roster by role now works as expected
* #31: Indicate online/offline status in Roster

### `0.1.5` - 06/10/2019

* Fix Guild Bank scanning logic
* DPS-only classes default to DPS role (Hunter, Warlock, Mage, Rogue)
* Automatically send player data on login
* Improve internal release process

### `0.1.4` - 05/10/2019

* Rework Guild Bank scanning logic
* Limit spam sending Guild Bank data
* Display Guild Bank Timestamp in Bank frame

### `0.1.3` - 05/10/2019

* Implement auto-scan Guild Bank data for Guild Bank alt
* Check requirements when setting Character Role to Guild Bank
* Show money & special class icon for Guild Bank alt in Roster
* Batch player skill update messages

### `0.1.2` - 05/20/2019

* Fix last class bar missing in Summary tab

### `0.1.1` - 04/10/2019

* Improve Roster Frame scrolling
* Intelligently update Roster Frame display
* Reorganize configuration options in preparatin for "Main Character" toggle
* Only show summary bars for classes in the guild

### `0.1.0` - 03/10/2019

* **NOTE** Guild members with older versions will not be able to update your data
* Reimplement almost the entire backend using AceDB as a base
* Existing data will be migrated to the new database format
* Add timestamp checks to all Guild Bank data transfers
* Allow mouse wheel scrolling of Roster and Guild frames
* Serialize/Deserialize player data

## `0.0`

### `0.0.16` - 22/09/2019

* add minimap button to open/close Guild Helper Classic or open the Blizzard options frame
* enable AceConfig option table validation

### `0.0.15` - 21/09/2019

* fix scangb not serializing data, only send item. ID and . Count

### `0.0.14` - 21/09/2019

* use AceConfig for options management
* revert to 0.0.9-beta delay for GUID and RegisterComm ordering=
* ensure GuildBank and GuildBankMoney also get initialized properly along with GuildDb and GuildDbBackup
* fix ChatThrottleLib embed errors

### `0.0.13` - 21/09/2019

* add missing AceEvent mixin
* fix sel: Debug typos
* improve /ghc help information
* TODO - add guild bank role option, maybe also bank alt and other?

### `0.0.11` - 20/09/2019

* add Debug print function and remove debug prints from release

### `0.0.10` - 20/09/2019

* inital work porting to Ace3
* fixed send bank data button not sending start frame
* fixed bank money initial display 0

### `0.0.7` - 19/09/2019

* the addon is becoming very messy and feels out of control, an overhaul will probably happen soon where the scripts and events are better sorted etc
* added alt info option
* added send bank data button
* added bank money info
* started work on options tab
* changed frame texture to grey style

### `0.0.6`

* started work on a new xml file for better UI, now using a tab system with resizing to follow
* added sort/filter options to roster

### `0.0.5` - 08/09/2019

* corrected guild bank scan check from 1 == 1 to if guildRank == 0 or guilRank == 1

### `0.0.4` - 08/09/2019

* added guild bank system
* added auto send character data sytem

### `0.0.3` - 07/09/2019

* added profession icon tooltip to guild roster listview items
* added slash command to run an addon settings reset option
* fixed issue where database wasnt created properly
* added check to guild scan so if no guild data available from game then nothing will run

### `0.0.2` - 06/09/2019

* bug fixed - character data now being parsed correctly
* updated addon database structure
* added version update system
