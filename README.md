GuildHelperClassic is designed for guilds and guild members, it allows players to share information with other guild members. Helps members find trade work and adventure companions.

Development Links
=================
- https://authors.curseforge.com/knowledge-base/projects/3451-automatic-packaging
- https://www.wowace.com/projects/ace3/pages/getting-started
- https://wow.gamepedia.com/WelcomeHome_-_Your_first_Ace3_Addon
- https://www.wowace.com/projects/libcompress?comment=20
- https://wowwiki.fandom.com/wiki/HOWTOs