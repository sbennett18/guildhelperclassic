local ADDON_NAME, GHC = ...
GHC_Player = {...}  -- Public interface
-- local Player = {...}  -- Private interface

function GHC_Player:UpdatePlayerDataTable()
    local guildspace = GHC_Core.Guildspace
    if not guildspace then return end

    local guid = UnitGUID("player")
    if not guid then return end

    -- locClass, engClass, locRace, engRace, gender, name, server
    local _, class, _, race, gender, name, _ = GetPlayerInfoByGUID(guid)

    local mainsName, role, oldPrimaryProfessions
    local oldPlayerTable = guildspace.Roster[guid]
    if oldPlayerTable then
        mainsName = oldPlayerTable.MainsName
        role = oldPlayerTable.Role
        if oldPlayerTable.Professions then
            oldPrimaryProfessions = oldPlayerTable.Professions.Primary
        end
    end

    local player = GHC_Core:CreateMemberTable({
        MainsName = mainsName or "-",
        Role = role or 0,
        Name = name,  -- TODO Don't serialize as it's available through GetGuildRosterInfo()
        Level = UnitLevel("player"),  -- TODO Don't serialize as it's available through GetGuildRosterInfo()
        Class = string.upper(class),  -- TODO Don't serialize as it's available through GetGuildRosterInfo()
        Gender = gender == 3 and "FEMALE" or "MALE",
        Race = string.upper(race),
    })

    -- TODO talent data isn't gathered at the moment and the intention is to avoid using it, instead players select their role(s) regardless of class/talents

    local skilledUp = false
    local p = 0
    for i = 1, GetNumSkillLines() do
        -- TODO Does this return a generic skill ID that doesn't need localization?
        local skill, _, _, level, _, _, _, _, _, _, _, _, _ = GetSkillLineInfo(i)
        -- TODO Couldn't this be easily extended to include Fishing, Cooking, First Aid?
        -- Don't need the Primary table distinction since it is known whether it's primary or not
        if GHC.Constants.Professions.Primary[skill] then
            p = p + 1
            if oldPrimaryProfessions then
                for _, profession in ipairs(oldPrimaryProfessions) do
                    if skill == profession.Name and level > profession.Level then
                        GHC_Core:Debug(skill, "+", level - profession.Level)
                        skilledUp = true
                    end
                end
            else
                skilledUp = true
            end
            -- TODO Key into Primary with skill name instead, use pairs iteration elsewhere as required
            player.Professions.Primary[p] = {
                Name = skill,
                Level = level,
            }
        end
    end

    guildspace.Roster[guid] = player
    return skilledUp
end


function GHC_Player:CreateMyDataString()
    local guid = UnitGUID("player")
    local player = GHC_Core.Guildspace.Roster[guid]
    return GHC_Core:Serialize(guid, player)
end


function GHC_Player:SendMyData()
    -- GHC_Core:Print("SendMyData", "ghc-playerdata")
    GHC_Core:SendCommMessage("ghc-playerdata", GHC_Player:CreateMyDataString(), "GUILD", nil, "BULK")
end


function GHC_Player:ParsePlayerData(ds, sender)
    if not GHC_Core.Guildspace then return end

    local success, guid, player = GHC_Core:Deserialize(ds)
    if success then
        GHC_Core.Guildspace.Roster[guid] = player
    else
        -- TODO Until update-me message is reworked, this is a hack -- alert once per sender per login
        GHC_Core.senders = GHC_Core.senders or {}
        if not GHC_Core.senders[sender] then
            GHC_Core.senders[sender] = true
            GHC_Core:Print(("Unable to parse data from %s - do they have an old version?"):format(sender))
            -- self:Debug(ds)
        end
        -- self:Print("error deserializing:", guid)
        -- self:Print("Make sure", GHC.addon_name, "is up to date!")
    end
end
