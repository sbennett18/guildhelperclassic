local name, GHC = ...


local function getPlayerData(entry)
    return function(info)
        local guildspace = GHC_Core.Guildspace
        if not guildspace then return nil end

        local guid = UnitGUID("player")
        if not guid then return nil end

        local player = guildspace.Roster[guid]
        if not player then return nil end

        local value = player[entry]
        return value
    end
end


local function setPlayerData(entry)
    return function(info, value)
        local guildspace = GHC_Core.Guildspace
        if not guildspace then return end

        local guid = UnitGUID("player")
        if not guid then return end

        local player = guildspace.Roster[guid]
        if not player then return end

        player[entry] = value

        GHC_Player:SendMyData()
    end
end


local options = {
    type = "group",
    -- get = function(info) GHC_Core:Print("get") end,
    -- set = function(info, val) GHC_Core:Print("set", val) end,
    args = {
        autoSendCharacterData = {
            order = 100,
            width = "full",
            name = "Auto send character data",
            desc = "Allowing this option will send your characters data whenever you gain a skill in a profession or level up!",
            type = "toggle",
            get = function(info)
                local val = GHC_Core.db.global.AutoSendCharacterData;
                return val;
            end,
            set = function(info, val)
                GHC_Core.db.global.AutoSendCharacterData = val;
            end,
        },
        currentCharacterOptions = {
            name = "Current Character",
            type = "group",
            order = 110,
            args = {
                guildBankAltWarning = {
                    order = 100,
                    type = "header",
                    name = "Guild Bank Alt",
                },
                guildBankCharacterRole = {
                    order = 101,
                    type = "description",
                    fontSize = "medium",
                    name = [[
In order to select the role of "Guild Bank", one of the following requirements must be met:
• The character's guild rank is Guild Master; or
• The character's guild rank is one level below Guild Master; or
• The character's guild rank is named "Guild Bank"; or
• The character's name is listed in the Guild Information window text in the following format:

GHC:^Charactername^
]]
                },
                characterRole = {
                    order = 102,
                    name = "Character role",
                    desc = "Select character role(s)",
                    type = "select",
                    get = getPlayerData("Role"),
                    set = function(info, value)
                        -- Check if guildRank is sufficient to be a Guild Bank
                        if value == #info.option.values then
                            if not GHC_Bank:IsPlayerEligibleGuildBank() then
                                GHC_Core:Printf(
                                    "WARNING: The Guild Bank character must be ranked %q, %q, or %q, or be listed in the Guild Information window, and must have its Character Role set to %q",
                                    GuildControlGetRankName(2), GuildControlGetRankName(1),  -- These are 1-indexed
                                    "Guild Bank", "Guild Bank"
                                )                                return
                            else
                                GHC_EventHandler:RegisterGuildBankEvents()
                            end
                        else
                            GHC_EventHandler:UnregisterGuildBankEvents()
                        end
                        return setPlayerData("Role")(info, value);
                    end,
                    values = {
                        -- nil,
                        "DPS",
                        "Healer",
                        "Tank",
                        "DPS-Healer",
                        "DPS-Tank",
                        "Healer-Tank",
                        "Any",
                        "Guild Bank",
                    }
                },
            },
        },
        mainCharacterOptions = {
            name = "Main Character",
            type = "group",
            order = 120,
            args = {
                isMainCharacter = {
                    order = 100,
                    width = "full",
                    name = "Main character",
                    desc = "Enable this option if this is your main character",
                    type = "toggle",
                    get = function(info)
                        local val = GHC_Core.db.char.IsMainCharacter
                        if val then
                            setPlayerData("MainsName")(info, UnitName("player"))
                        end
                        return val
                    end,
                    set = function(info, val)
                        GHC_Core.db.char.IsMainCharacter = val
                        setPlayerData("MainsName")(info, UnitName("player"))
                    end,
                },
                mainCharacterName = {
                    order = 110,
                    name = "Main character name",
                    desc = "Enter your main's name",
                    type = "input",
                    multiline = false,
                    get = getPlayerData("MainsName"),
                    set = setPlayerData("MainsName"),
                    disabled = function() return GHC_Core.db.char.IsMainCharacter; end,
                },
            },
        },
    }
}

local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
AceConfigRegistry:RegisterOptionsTable(name, options, false)

local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local f = AceConfigDialog:AddToBlizOptions(name, GHC.addon_name, nil)