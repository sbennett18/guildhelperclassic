local ADDON_NAME, GHC = ...
-- GHC_EventHandler = {...}
GHC_EventHandler = {}

local tinsert = table.insert
local InCombatLockdown = _G.InCombatLockdown

local playerDataQueue = {}
local isMyDataChanged = false


function GHC_EventHandler:GuildBankMoneyCommHandler(prefix, message, distribution, sender)
    GHC_Core:Debug("GuildBankMoneyCommHandler", sender, prefix, distribution, string.len(message))
    GHC_Core:Debug(message)

    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC_Bank:GuildBankStart(message, sender)
end


function GHC_EventHandler:GuildBankDataCommHandler(prefix, message, distribution, sender)
    GHC_Core:Debug("GuildBankDataCommHandler", sender, prefix, distribution, string.len(message))
    -- GHC_Core:Debug(message)

    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named since
    -- AceComm handles packetizing large messages automatically
    GHC_Bank:ParseGuildBankData(message, sender)
end


function GHC_EventHandler:GuildBankFinishCommHandler(prefix, message, distribution, sender)
    GHC_Core:Debug("GuildBankFinishCommHandler", sender, prefix, distribution, string.len(message))
    GHC_Core:Debug(message)

    -- TODO Rework this set of 3 functions (Start, Parse, Finish) to be more aptly named
    GHC_Bank:GuildBankFinish(message, sender)
end


function GHC_EventHandler:UpdateMeCommHandler(prefix, message, distribution, sender)
    GHC_Core:Debug("UpdateMeCommHandler", sender, prefix, distribution, string.len(message))
    GHC_Core:Debug(message)

    if GHC_Core.db.global.CurrentVersion ~= message then
        GHC_Core:Print("New version available!")
        GHC_Core.db.global.NewVersion = message
    end
end


--- Default Comm handler
function GHC_EventHandler:OnCommReceived(prefix, message, distribution, sender)
    GHC_Core:Debug("OnCommReceived", sender, prefix, distribution, string.len(message))
    GHC_Core:Debug(message)
end


-- GetGuildInfo("player") should be available now
function GHC_EventHandler:GUILD_ROSTER_UPDATE(eventName, canRequestRosterUpdate)
end


-- GetNumGuildMembers() is available now
function GHC_EventHandler:GUILD_RANKS_UPDATE(eventName)
    -- Only need to register the Guildspace once
    if GHC_Core:RegisterGuildspace(GHC_Core:GetPlayerGuildInfo()) then
        GHC_Core:UnregisterEvent(eventName)

        GHC_Core:Load()

        local guid = UnitGUID("player")
        local player = GHC_Core.Guildspace.Roster[guid]
        if not player then return end

        if not GHC_Core.db.char.IsMainCharacter and (player.MainsName == nil or strtrim(player.MainsName) == "") then
            GHC_Core:Print("Please set your Main Character in the addon configuration window")
        end
        if player.Role == nil then
            GHC_Core:Print("Please select your Role in the addon configuration window")
        end

        local isPlayerEligibleGuildBank = GHC_Bank:IsPlayerEligibleGuildBank(guid)

        -- Only register Guild Bank events if you are the Guild Bank Alt
        if player.Role == #GHC.Db.RoleIconTextString then
            if isPlayerEligibleGuildBank then
                GHC_EventHandler:RegisterGuildBankEvents()
            else
                GHC_Core:Printf("WARNING: %q no longer meets the requirements of a Guild Bank alt", player.Name)
                GHC_Core:Printf(
                    "WARNING: The Guild Bank character must be ranked either %q or %q or be listed in the Guild Information window and must have its Character Role set to %q",
                    GuildControlGetRankName(2), GuildControlGetRankName(1),  -- These are 1-indexed
                    "Guild Bank"
                )
                GHC_Core:Print("If you were recently promoted or listed in the Guild Information window as a Guild Bank alt then you must log out and back in")
            end
        end
        GHC_Player:UpdatePlayerDataTable()
        GHC_Player:SendMyData()  -- Always send Player Data on login
    end
end


function GHC_EventHandler:PLAYER_REGEN_ENABLED(eventName)
    local c = 0
    for _ in pairs(playerDataQueue) do
        c = c + 1
    end
    -- GHC_Core:Debug("<<<", eventName, "=", c)
    -- GHC_Core:Debug("SendMyData", isMyDataChanged)
    GHC_Core:Debug("Process queued data", c, isMyDataChanged)
    if isMyDataChanged then
        local isDataChanged = GHC_Player:UpdatePlayerDataTable()
        -- GHC_Core:Debug(isDataChanged)
        if isDataChanged then
            GHC_Player:SendMyData()
        end
        isMyDataChanged = false
    end
    for sender, message in pairs(playerDataQueue) do
        -- GHC_Core:Debug(sender)
        -- If combat is reentered then stop parsing data
        if InCombatLockdown() then break end

        GHC_Player:ParsePlayerData(message, sender)
        playerDataQueue[sender] = nil
        c = c - 1
        -- GHC_Core:Debug(message)
    end
    -- GHC_Core:Debug(">>>", eventName, "=", c)
end


function GHC_EventHandler:PlayerDataCommHandler(prefix, message, distribution, sender)
    -- If in combat queue up player updates until leaving combat, only storing the most recent update
    if InCombatLockdown() then
        GHC_Core:Debug("Queue player data from", sender)
        playerDataQueue[sender] = message
    else
        GHC_Player:ParsePlayerData(message, sender)
    end
end


function GHC_EventHandler:PlayerDataChangedHandler()
    -- GHC_Core:Debug("PlayerDataChangedHandler")
    if GHC_Core.db.global.AutoSendCharacterData then
        if InCombatLockdown() then
            GHC_Core:Debug("Queue send my data")
            isMyDataChanged = true
        else
            if GHC_Player:UpdatePlayerDataTable() then
                GHC_Player:SendMyData()
            end
        end
    end
end

function GHC_EventHandler:SKILL_LINES_CHANGED(eventName)
    -- GHC_Core:Debug(eventName)
end


--- TODO Reset Guildspace and/or register new Guildspace
function GHC_EventHandler:PLAYER_GUILD_UPDATE(eventName)
    GHC_Core:Print("Please logout and back in to fix Guild Helper Classic!")
    local guildName = GHC_Core:GetPlayerGuildInfo()
    if guildName then
    end
end


function GHC_EventHandler:BANKFRAME_OPENED(eventName)
    -- GHC_Core:Print(eventName, GHC_Bank.isBankOpen, "-->", true, GHC_Bank.updateBagTimerId, "-->", nil)
    -- Wait until Bank Frame closes (BANKFRAME_CLOSED) to ScanGuildBank
    GHC_Bank.isBankOpen = true
    -- GHC_Bank:CancelUpdateBagTimer()
end


function GHC_EventHandler:BANKFRAME_CLOSED(eventName)
    -- GHC_Core:Print(eventName, GHC_Bank.isBankOpen)
    -- Ensure we only acknowledge the first BANKFRAME_CLOSED event
    if GHC_Bank.isBankOpen then
        -- GHC_Bank:CancelUpdateBagTimer()
        if GHC_Bank:ScanGuildBank() ~= nil then
            GHC_Bank:SendGuildBankData()
        end
    end
    GHC_Bank.isBankOpen = false
end


function GHC_EventHandler:RegisterGuildBankEvents()
    --@debug@
    -- GHC_Core:Debug("RegisterGuildBankEvents", GHC_Bank.areGuildBankEventsRegistered)
    --@end-debug@
    if GHC_Bank.areGuildBankEventsRegistered then return end

    GHC_EventHandler:RegisterEvent("BANKFRAME_OPENED")
    GHC_EventHandler:RegisterEvent("BANKFRAME_CLOSED")
    -- GHC_EventHandler:RegisterEvent("BAG_UPDATE", "UpdateBags")
    -- GHC_EventHandler:RegisterEvent("PLAYER_MONEY", "UpdateBags")
    GHC_Bank.areGuildBankEventsRegistered = true
end


function GHC_EventHandler:UnregisterGuildBankEvents()
    -- GHC_Core:Debug("UnegisterGuildBankEvents", GHC_Bank.areGuildBankEventsRegistered)
    if not GHC_Bank.areGuildBankEventsRegistered then return end
    GHC_Bank.areGuildBankEventsRegistered = false

    -- Cancel any pending updates
    GHC_Bank:CancelUpdateBagTimer()

    GHC_EventHandler:UnregisterEvent("BANKFRAME_OPENED")
    GHC_EventHandler:UnregisterEvent("BANKFRAME_CLOSED")
    -- GHC_EventHandler:UnregisterEvent("BAG_UPDATE")
    -- GHC_EventHandler:UnregisterEvent("PLAYER_MONEY")
end


function GHC_EventHandler:RegisterEvent(eventName, callback)
    if not callback then callback = eventName end
    GHC_Core:RegisterEvent(eventName, function(...) self[callback](self, ...) end)
end

function GHC_EventHandler:UnregisterEvent(...) GHC_Core:UnregisterEvent(...) end


function GHC_EventHandler:RegisterBucketEvent(events, timeout, callback)
    GHC_Core:RegisterBucketEvent(events, timeout, function(...) self[callback](self, ...) end)
end


function GHC_EventHandler:RegisterComm(prefix, callback)
    GHC_Core:RegisterComm(prefix, function(...) self[callback](self, ...) end)
end